import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';

const appRoutes : Routes = [
  { path: '', component: HomeComponent }
 //  { path: '', component: MainComponent,
 //    children: [
 //      { path: 'Dashboard', component: DashboardComponent },
 //      { path: 'Pacientes', component: PacientesComponent },
 //      { path: 'Pacientes/:id', component: PerfilComponent },
 //      { path: 'Finanzas', component: FinanzasComponent },
 //      { path: 'Agenda', component: AgendaComponent }
 //
 //    ]
 // }
];

export const appRoutingProviders: any[] = [];
export const routing : ModuleWithProviders = RouterModule.forRoot(appRoutes);
