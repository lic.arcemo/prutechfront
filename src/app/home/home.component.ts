import { Component, OnInit } from '@angular/core';
import { _ } from 'underscore';
import { NgForm,FormGroup, FormArray, FormBuilder, Validators,ReactiveFormsModule } from '@angular/forms';
import { Response, Headers, RequestOptions,Http} from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  public numeros= new Array();
  public inicial;
  public final;
  public numerosEncontrados = 0;

  constructor( private http:HttpClient ) {
    this.inicial = 1;
    this.final = 0;
  }

  ngOnInit() {

  }

  checkPrimo( numero ) {
    for (var i = 2; i < numero; i++) {

      if (numero % i === 0) {
        return false;
      }
    }
    return numero !== 1;
  }

  getNumeros(inicial, final){
    var primos = [];
    var end = final +1;

    if(inicial>200 || final>200){
      alert("Los valores no deben superar el número 200");
    }
    else{
      this.numeros= _.range(inicial,end,1);

      for (var i=0; i<this.numeros.length; i++ ){
        if (this.checkPrimo(this.numeros[i])) {
          primos.push(this.numeros[i]);
        }
      }
      this.numerosEncontrados = primos.length;
      this.sendNumeros(primos);
    }
  }

  calcularPrimos(){
    if(this.inicial<1){
      alert("el numero inicial no debe ser 0");
    }
    else{
      this.getNumeros(this.inicial, this.final);
    }
  }
  sendNumeros(primos){
      console.log(primos);
      var datos = {
        inicial: this.inicial,
        final: this.final,
        cantidadNumeros: primos.length,
        arreglodatos: JSON.stringify(primos)
      }

      if (primos.length>=100){
        alert("La cantidad de numeros primos son: "+primos.length);
      }
      else{
        this.http.post( 'http://localhost:3000/api/Primos',datos)
        .toPromise()
        .then((res) => {
          alert("Los numeros primos fueron guardados correctamente: "+primos.length);
        })
        .catch( err => {
           console.log(err)
        });
        this.numeros = primos;
      }
  }


}
